package com.example.demo.controllers

import com.example.demo.services.SleepService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.atomic.AtomicInteger
import kotlin.concurrent.thread
import kotlin.math.floor

@RestController
@RequestMapping("/")
class RestController {

    @Autowired
    private lateinit var service: SleepService

    val cnt = AtomicInteger(0)

    @GetMapping("/")
    fun get(): String {
        val c = cnt.addAndGet(1)
        thread { service.sleep(floor(Math.random() * 20 + 20).toLong(), c) }
        return "ok %d\n".format(c)
    }

    @PostMapping("/")
    fun post(): String {
        val c = cnt.addAndGet(1)
        thread { service.sleep(floor(Math.random() * 20 + 20).toLong(), c) }
        return "ok %d\n".format(c)
    }
}
