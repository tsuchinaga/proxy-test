package com.example.demo.logger

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.LoggerContext
import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.rolling.RollingFileAppender
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.nio.charset.StandardCharsets

@Service
class Logger {

    private val log = newLogger("log")

    fun getLogger(): Logger = log

    private fun newLogger(name: String): Logger {
        val loggerContext = LoggerFactory.getILoggerFactory() as LoggerContext

        val fileAppender = RollingFileAppender<ILoggingEvent>()

        val rollingPolicy = TimeBasedRollingPolicy<ILoggingEvent>()
        rollingPolicy.fileNamePattern = "logs/$name.log.%d{yyyy-MM-dd}.gz"
        rollingPolicy.maxHistory = 14
        rollingPolicy.setParent(fileAppender)
        rollingPolicy.context = loggerContext
        rollingPolicy.isCleanHistoryOnStart = true
        rollingPolicy.start()
        fileAppender.rollingPolicy = rollingPolicy

        val encoder = PatternLayoutEncoder()
        encoder.context = loggerContext
        encoder.pattern = "%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %r %-5level %logger{36} [%file:%line] - %msg%n"
        encoder.charset = StandardCharsets.UTF_8
        encoder.start()
        fileAppender.encoder = encoder

        fileAppender.isAppend = true
        fileAppender.context = loggerContext
        fileAppender.name = name
        fileAppender.file = "logs/$name.log"
        fileAppender.start()

        // attach the rolling file appender to the logger of your choice
        val logger = loggerContext.getLogger(name)
        logger.addAppender(fileAppender)
        logger.isAdditive = false
        logger.level = Level.INFO
        return logger
    }
}
