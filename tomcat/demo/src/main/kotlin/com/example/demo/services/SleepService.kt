package com.example.demo.services

import com.example.demo.logger.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class SleepService {
    @Autowired
    private lateinit var logger: Logger

    fun sleep(seconds: Long, count: Int) {
        val start = LocalDateTime.now()
        val log = logger.getLogger()
        Thread.sleep(seconds * 1000, 0)
        log.info("%s - %s ok %d".format(start, LocalDateTime.now(), count))
    }
}
